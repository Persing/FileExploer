package com.thesoftwarepersing.fileexplorerandencrypt;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.thesoftwarepersing.fileexploerandencryptor.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        //set up toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Check that permissions allow the device to read filesystem
        checkPermission();
    }

    //add overflow menu to activity
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    //handle the menu selection
    //TODO: implement
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem){
        switch (menuItem.getItemId()){
            case R.id.action_list:
                MainActivityFragment.type = "list";
                break;
            case R.id.action_grid:
//                MainActivityFragment.type = "grid";
//                Bundle args = new Bundle();
//                args.putString("dir", .getPath());
//
//                MainActivityFragment nextFrag = new MainActivityFragment();
//                nextFrag.setArguments(args);
//
//                ((MainActivity) mContext).getSupportFragmentManager().beginTransaction()//.addToBackStack(null)
//                        .replace(R.id.fragment, nextFrag)
//                        .addToBackStack(null)
//                        .commit();
                break;
            default:
                return true;
        }
        return true;
    }


    @Override
    public void onBackPressed(){
        //This is a bit of a "hack" to create expected behaviour
        //for some reason when using back stack to retrieve previous fragments, the first
        //fragment could not reliably retrieved. I believe that is because the first fragment
        //was not added in the same fashion as the others.
        if(getSupportFragmentManager().getBackStackEntryCount() <=1){
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment,new MainActivityFragment()).commit();
        }else {
            getSupportFragmentManager().popBackStackImmediate();
        }
    }

    //check and if not already present request permission to read and write external storage
    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE},
                    123);
        }
    }
}

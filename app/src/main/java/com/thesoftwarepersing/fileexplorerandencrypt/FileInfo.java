package com.thesoftwarepersing.fileexplorerandencrypt;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thesoftwarepersing.fileexploerandencryptor.R;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by nickpersing on 1/17/18.
 */

public class FileInfo extends DialogFragment{
    //on second look
    String mFileName, mFilePath, mLastModified, mFileSize;

    //Create new instance of dialog frag and generate the args needed
    static FileInfo newInstance(String fileName, String filePath,
                                String lastModified, String fileSize){
        FileInfo f = new FileInfo();
        Bundle args = new Bundle();
        args.putString("fileName", fileName);
        args.putString("filePath", filePath);
        args.putString("lastModified", lastModified);
        args.putString("fileSize", fileSize);
        f.setArguments(args);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        mFileName = getArguments().getString("fileName");
        mFilePath = getArguments().getString("filePath");
        mLastModified = getArguments().getString("lastModified");
        mFileSize = getArguments().getString("fileSize");

    }

    @Override
    public Dialog onCreateDialog(Bundle savedStateInstance){
        String message = "File Name : " + getArguments().getString("fileName") + "\n"
                + "File Path: " + getArguments().getString("filePath") + "\n"
                + "File Size: " + getArguments().getString("fileSize") + "B" + "\n"
                + "Last Modified: " + getArguments().getString("lastModified");
        return new AlertDialog.Builder(getActivity())
                .setTitle(getArguments().getString("fileName"))
                .setMessage(message)
                .setPositiveButton(R.string.encrypt, new DialogInterface.OnClickListener()
                {   //Handle the encryption of the file
                    //TODO: in the future change to make user create a key
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        SecretKeySpec sks = new SecretKeySpec("tempPasswordHolder".getBytes(), "AES");
                        try {
                            FileInputStream fis = new FileInputStream(
                                    new File(getArguments().getString("filePath")));
                            FileOutputStream fos = new FileOutputStream(
                                    new File(getArguments().getString("filePath")+"_encrypted"));
                            //TODO: debug this issue
                            //java.security.NoSuchAlgorithmException: No provider found for AES_256/CBC/PKCS5Padding
                            //java.security.NoSuchAlgorithmException: No provider found for AES_256/CBC/NoPadding
                            Cipher cipher = Cipher.getInstance("AES_256/CBC/PKCS5Padding");
                            cipher.init(Cipher.ENCRYPT_MODE, sks);
                            CipherOutputStream cos = new CipherOutputStream(fos, cipher);
                            byte[] b = new byte[8];
                            int i = fis.read(b);
                            while (i != -1) {
                                cos.write(b, 0, i);
                                i = fis.read(b);
                            }
                            cos.flush();
                            cos.close();
                            fos.close();
                            fis.close();
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        } catch (NoSuchPaddingException e) {
                            e.printStackTrace();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (InvalidKeyException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                })
                .setNegativeButton(R.string.decrypt, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SecretKeySpec sks = new SecretKeySpec("tempPasswordHolder".getBytes(), "AES");
                        try {// handle decryption
                             //TODO: alow user to enter key and make a method to check if the file is encrypted or not
                            FileInputStream fis = new FileInputStream(
                                    new File(getArguments().getString("filePath")));
                            FileOutputStream fos = new FileOutputStream(
                                    new File(getArguments().getString("filePath")+"_decrypted"));
                            Cipher cipher = Cipher.getInstance("AES_256/CBC/PKCS5Padding");
                            cipher.init(Cipher.DECRYPT_MODE, sks);
                            CipherInputStream cis = new CipherInputStream(fis, cipher);
                            int b;
                            byte[] d = new byte[8];
                            while((b = cis.read(d)) != -1) {
                                fos.write(d, 0, b);
                            }
                            fos.flush();
                            fos.close();
                            cis.close();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        } catch (NoSuchPaddingException e) {
                            e.printStackTrace();
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        } catch (InvalidKeyException e) {
                            e.printStackTrace();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                    }
                })
                .create();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_frag, container,
                false);

        getDialog().setTitle(mFileName + " - Properties");

//        String name = "File Name: " + mFileName;
//        ((TextView)getDialog().findViewById(R.id.dialog_file_name)).setText(name);
//        ((TextView)getActivity().findViewById(R.id.dialog_file_name)).setText(name);
//        ((TextView)getActivity().findViewById(R.id.dialog_file_path)).setText("File Path: " + mFilePath);
//        ((TextView)getActivity().findViewById(R.id.dialog_last_modified)).setText("Last Modified: " + mLastModified);
//        ((TextView)getActivity().findViewById(R.id.dialog_file_size)).setText("File Size: " + mFileSize);

        return rootView;
    }
}

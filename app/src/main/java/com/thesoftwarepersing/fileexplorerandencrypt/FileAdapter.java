package com.thesoftwarepersing.fileexplorerandencrypt;

import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.thesoftwarepersing.fileexploerandencryptor.R;

import java.io.File;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by nickpersing on 1/15/18.
 */

public class FileAdapter extends RecyclerView.Adapter<FileAdapter.FileViewHolder> {

    private List<File> fileList;
    public Context mContext;

    public FileAdapter(Context context, File file, String type){
        this.fileList = new ArrayList<File>();
        if(file.getParent() != null)
            this.fileList.add(new File(file.getParent()));
        this.fileList.addAll(Arrays.asList(file.listFiles()));

        mContext = context;
    }

    @Override
    public FileAdapter.FileViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.file_list_row, parent, false);
        return new FileViewHolder(itemView);
    }

    //use holder class to attach specific element pieces to list item
    @Override
    public void onBindViewHolder(FileAdapter.FileViewHolder holder, int position) {
        File file = fileList.get(position);
        holder.file = file;
        holder.fileName.setText(file.getName());
        holder.fileSize.setText("size: " + file.length() + "B");
        //This sets each preview for each file whether it
        // is a folder, file or creates a thumbnail for images
        if(isImageFile(file.getName())){
            //inefficient for large file sizes better to load scaled image
            //TODO: load scaled images instead of this method
            holder.filePreview.setImageBitmap(
                    ThumbnailUtils.extractThumbnail(
                            BitmapFactory.decodeFile(file.getPath()),100,100));
        } else if(file.isFile()){
            holder.filePreview.setImageResource(R.drawable.file);
        }  else{
            holder.filePreview.setImageResource(R.drawable.folder);
        }

    }

    //test to se if file is an image
    private boolean isImageFile(String file) {
        String ext = file.substring(file.lastIndexOf(".") + 1);

        if (ext.equalsIgnoreCase("png") || ext.equalsIgnoreCase("jpg") ||
                ext.equalsIgnoreCase("jpeg")|| ext.equalsIgnoreCase("gif") ||
                ext.equalsIgnoreCase("tiff")|| ext.equalsIgnoreCase("tif"))
            return true;

        return false;
    }

    @Override
    public int getItemCount() {
        return fileList.size();
    }

    //class to bind xml elements to code placeholders
    public class FileViewHolder extends RecyclerView.ViewHolder{
        TextView fileName, fileSize;
        ImageView filePreview;
        File file;
        public FileViewHolder(View view){
            super(view);
            fileName = view.findViewById(R.id.dialog_file_name);
            fileSize = view.findViewById(R.id.dialog_file_size);
            filePreview = view.findViewById(R.id.file_preview);

            //Short click to open directory
            //TODO: add opening file preview start new Intent with MIME type guessed by file extension
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(file.isDirectory()) {
                        //new fragment at target directory to replace current one
                        Bundle args = new Bundle();
                        args.putString("dir", file.getPath());
                        MainActivityFragment nextFrag = new MainActivityFragment();
                        nextFrag.setArguments(args);
                        ((MainActivity) mContext).getSupportFragmentManager().beginTransaction()//.addToBackStack(null)
                                .replace(R.id.fragment, nextFrag)
                                .addToBackStack(null)
                                .commit();
                    }
                }
            });

            //long click to launch fileinfo dialog frag
            view.setOnLongClickListener((new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    FragmentManager fm = ((MainActivity) mContext).getSupportFragmentManager();
                    FileInfo f = FileInfo.newInstance(file.getName(),
                            file.getPath(),
                            new SimpleDateFormat("MM/dd/yyyy").format(new Date(file.lastModified())),
                            String.valueOf(file.length()));
                    f.show(fm, "dialog");
                    return false;
                }
            }));


        }
    }
}

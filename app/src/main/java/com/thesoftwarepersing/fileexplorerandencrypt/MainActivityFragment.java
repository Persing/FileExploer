package com.thesoftwarepersing.fileexplorerandencrypt;

import android.os.Environment;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thesoftwarepersing.fileexploerandencryptor.R;

import java.io.File;

/**
 * Created by nickpersing on 1/15/18.
 */
public class MainActivityFragment extends Fragment {

    //ListView mFileList;
    public static String type = "list";
    RecyclerView mFileList;
    File dir;
    FileAdapter mAdapter;

    public MainActivityFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        //check that Arguments have been passed to the Fragment
        //Then check to see if that Argument is "dir" the directory to open in the new Fragment
        if(getArguments() != null && getArguments().containsKey("dir")){
            dir = new File(getArguments().getString("dir"));
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
        mFileList = getActivity().findViewById(R.id.file_list);

        //Below are two methods for changing between grid both work, but the difficulty
        //is changing the layout used for switching the list items between grid and list
        //Set layout to desired type
        RecyclerView.LayoutManager mLayoutManager;
        if(type.equals("grid")){
            mLayoutManager = new GridLayoutManager(getContext(),2);
        }else{
            mLayoutManager = new LinearLayoutManager(getContext());
        }
//        if(getArguments() != null && getArguments().getString("type") != null
//                && getArguments().getString("type").equals("grid")){
//                mLayoutManager = new GridLayoutManager(getContext(),2);
//        }else{
//            mLayoutManager = new LinearLayoutManager(getContext());
//        }

        mFileList.setLayoutManager(mLayoutManager);
        mFileList.setItemAnimator(new DefaultItemAnimator());

        //if the dir passed in args is unavailable then just go to root directory
        //which actually returns "/system"
        //TODO: add error message
        if(dir == null || !dir.canRead()) {
            dir = Environment.getRootDirectory();
        }


        mAdapter = new FileAdapter(getActivity(), dir, type);

        mFileList.setAdapter(mAdapter);

        getActivity().setTitle(dir.getName());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    //placed as part of attempt to for switching between list and grid view
    //the idea was to give a way for the main activity to "read" the current directory and
    //reluanch the activity with changes to layout but resume the last location
    public File getDir(){
        return dir;
    }
}
